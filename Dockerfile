FROM node:16-bullseye

WORKDIR /usr/src/app

RUN apt-get update -yqq && apt-get install -yqq ghostscript

COPY . .
RUN yarn install

EXPOSE 80

CMD ["yarn", "run", "start"]
import express, { ErrorRequestHandler } from "express"
import "express-async-errors"
import compression from "compression"
import prettyBytes from "pretty-bytes"
import morgan from "morgan"
import multer from "multer"
import expressBasicAuth from "express-basic-auth"
import createError from "http-errors"
import PdfShrinker from "./PdfShrinker"

const app = express()
const port = process.env.PDF_SHRINK_SERVER_PORT ?? 80

app.disable("x-powered-by")

const upload = multer({ storage: multer.memoryStorage(), limits: { fileSize: 500 * 1024 * 1024 /* 500Mb */ } })

app.use(morgan("dev"))
app.use(compression({ filter: () => true }))

// Auth
const username = process.env.PDF_SHRINK_SERVER_USERNAME
const secret = process.env.PDF_SHRINK_SERVER_SECRET
if (username != null && secret != null) {
	app.use(expressBasicAuth({ users: { [username]: secret } }))
}

app.post("/shrink", upload.single("pdf"), async (req, res) => {
	const pdf = req.file!
	if (pdf == null) throw new createError.UnprocessableEntity("Missing pdf file (Max size: 500Mb)")
	if (pdf.mimetype !== "application/pdf")
		throw new createError.UnprocessableEntity("Invalid mimetype (only 'application/pdf' is accepted)")

	console.log(`pdf size ${prettyBytes(pdf.size)}`)

	let shrinked
	if (req.body.maxSize == null) {
		shrinked = await PdfShrinker.shrinkBuffer(pdf.buffer)
	} else {
		const maxSize = Number(req.body.maxSize)
		if (!Number.isInteger(maxSize)) throw new createError.UnprocessableEntity("maxSize is not an interger")

		console.log(`maxSize ${prettyBytes(maxSize)}`)

		shrinked = await PdfShrinker.shrinkUnderSizeBuffer(pdf.buffer, maxSize)
	}

	res.header("Access-Control-Allow-Origin", "*")
	res.contentType("application/pdf")
	res.end(shrinked)

	console.log(`Shrunk pdf file from ${prettyBytes(pdf.size)} to ${prettyBytes(shrinked.byteLength)}`)
})

// Express detects an error handler as a function having 4 arguments, we need to keep `next` even if unused
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
	console.error(err)

	if (createError.isHttpError(err)) {
		res.status(err.statusCode).end(err.message)
	} else {
		res.status(500).end("Unknown error")
	}
}
app.use(errorHandler)

app.listen(port, () => {
	console.log(`Listening on ${port}`)
})

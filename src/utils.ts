import { spawn, SpawnOptionsWithoutStdio } from "child_process"

const logCommands = process.env.PDF_SHRINK_SERVER_LOG_COMMANDS === "true"

export function spawnPromise(
	command: string,
	args: string[],
	option?: SpawnOptionsWithoutStdio
): Promise<number | null> {
	return new Promise((resolve) => {
		const process = spawn(command, args, option)

		if (logCommands) {
			console.log(`${command} ${args.join(" ")}`)
			process.stdout.on("data", (b: Buffer) => console.log(b.toString()))
			process.stderr.on("data", (b: Buffer) => console.error(b.toString()))
		}

		process.on("close", (code) => {
			resolve(code)
		})
	})
}

import fs from "fs/promises"
import _ from "lodash"
import createError from "http-errors"
import prettyBytes from "pretty-bytes"
import { file } from "tmp-promise"
import { spawnPromise } from "./utils"

export default class PdfShrinker {
	private static async changeDpiFile(inputFilepath: string, outputFilepath: string, dpi = 100): Promise<void> {
		const code = await spawnPromise("gs", [
			"-q",
			"-dNOPAUSE",
			"-dBATCH",
			"-dSAFER",
			"-sDEVICE=pdfwrite",
			"-dCompatibilityLevel=1.4",
			"-dPDFSETTINGS=/screen",
			"-dEmbedAllFonts=true",
			"-dSubsetFonts=true",
			"-dAutoRotatePages=/None",
			"-dColorImageDownsampleType=/Bicubic",
			`-dColorImageResolution=${dpi}`,
			"-dGrayImageDownsampleType=/Bicubic",
			`-dGrayImageResolution=${dpi}`,
			"-dMonoImageDownsampleType=/Subsample",
			`-dMonoImageResolution=${dpi}`,
			`-sOutputFile="${outputFilepath}"`,
			`"${inputFilepath}"`,
		])

		if (code !== 0) throw new createError.InternalServerError("Ghostscript error")
	}

	private static async changeDpiBuffer(pdf: Buffer, dpi = 100): Promise<Buffer> {
		const inputFile = await file({ prefix: "dpi_input_", postfix: ".pdf" })
		const outputFile = await file({ prefix: "dpi_ouput_", postfix: ".pdf" })

		try {
			await fs.writeFile(inputFile.path, pdf)

			await PdfShrinker.changeDpiFile(inputFile.path, outputFile.path, dpi)

			return await fs.readFile(outputFile.path)
		} finally {
			await inputFile.cleanup()
			await outputFile.cleanup()
		}
	}

	private static async changeDpiUntilSizeUnderFile(
		inputFilepath: string,
		outputFilepath: string,
		maxSizeInBytes: number,
		startingDpi = 100
	) {
		// Try lower and lower DPI until being under the max filesize
		let dpi = startingDpi
		do {
			if (dpi <= 0) {
				throw new createError.InternalServerError(`Unable to shrink pdf under ${prettyBytes(maxSizeInBytes)}`)
			}

			await PdfShrinker.changeDpiFile(inputFilepath, outputFilepath, dpi)
			dpi -= 10
		} while ((await fs.stat(outputFilepath)).size > maxSizeInBytes)
	}

	private static async changeDpiUntilSizeUnderBuffer(
		pdf: Buffer,
		maxSizeInBytes: number,
		startingDpi = 100
	): Promise<Buffer> {
		const inputFile = await file({ prefix: "dpi_until_input_", postfix: ".pdf" })
		const outputFile = await file({ prefix: "dpi_until_ouput_", postfix: ".pdf" })

		try {
			await fs.writeFile(inputFile.path, pdf)

			await PdfShrinker.changeDpiUntilSizeUnderFile(inputFile.path, outputFile.path, maxSizeInBytes, startingDpi)

			return await fs.readFile(outputFile.path)
		} finally {
			await inputFile.cleanup()
			await outputFile.cleanup()
		}
	}

	private static async convertToPsAndBackToPdfFile(inputFilepath: string, outputFilepath: string): Promise<void> {
		const psFile = await file({ prefix: "convert_ps_", postfix: ".ps" })

		try {
			await spawnPromise("pdf2ps", [inputFilepath, psFile.path])
			await spawnPromise("ps2pdf", [psFile.path, outputFilepath])
		} finally {
			await psFile.cleanup()
		}
	}

	private static async convertToPsAndBackToPdfBuffer(pdf: Buffer): Promise<Buffer> {
		const inputFile = await file({ prefix: "convert_input_", postfix: ".pdf" })
		const outputFile = await file({ prefix: "convert_ouput_", postfix: ".pdf" })

		try {
			await fs.writeFile(inputFile.path, pdf)

			await PdfShrinker.convertToPsAndBackToPdfFile(inputFile.path, outputFile.path)

			return await fs.readFile(outputFile.path)
		} finally {
			await inputFile.cleanup()
			await outputFile.cleanup()
		}
	}

	public static async shrinkBuffer(pdf: Buffer): Promise<Buffer> {
		const [dpiChanged, converted] = await Promise.all([
			PdfShrinker.changeDpiBuffer(pdf),
			PdfShrinker.convertToPsAndBackToPdfBuffer(pdf),
		])

		return _.minBy([pdf, dpiChanged, converted], "byteLength") as Buffer
	}

	public static async shrinkUnderSizeBuffer(pdf: Buffer, maxSizeInBytes: number): Promise<Buffer> {
		const converted = await PdfShrinker.convertToPsAndBackToPdfBuffer(pdf)
		if (converted.byteLength < maxSizeInBytes) {
			return converted
		}

		return PdfShrinker.changeDpiUntilSizeUnderBuffer(pdf, maxSizeInBytes)
	}
}

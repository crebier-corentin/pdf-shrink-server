# PDF Shrink server

HTTP server using [ghostscript](https://www.ghostscript.com/) to shrink pdf files.

## Usage

There is only one route :

```
POST /shrink
```

Expecting `multipart/form-data` request content type.

| field   | type    | description                                                           |
| ------- | ------- | --------------------------------------------------------------------- |
| pdf     | file    | PDF file to shrink.                                                   |
| maxSize | integer | **Optional** Maximum size (in bytes) that the returned pdf should be. |

If `maxSize` is specified, the server will try to reduce the dpi until the file size is under `maxSize`. If it cannot, it will return a 500 error instead.

## Config

| Environement variable          | default | description                                                                               |
| ------------------------------ | ------- | ----------------------------------------------------------------------------------------- |
| PDF_SHRINK_SERVER_LOG_COMMANDS | false   | `true` to log commands (ghostscript, pdftk) and their output to console.                  |
| PDF_SHRINK_SERVER_PORT         | 80      | The port the server will listen to.                                                       |
| PDF_SHRINK_SERVER_USERNAME     |         | Username for HTTP Basic Auth. **If not present, no auth check will be performed.**        |
| PDF_SHRINK_SERVER_SECRET       |         | Password/secret for HTTP Basic Auth. **If not present, no auth check will be performed.** |

If both `PDF_SHRINK_SERVER_USERNAME` and `PDF_SHRINK_SERVER_SECRET` are set, the server will return a 401 error if a correct `Authorization` header is not present.
